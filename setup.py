#!/usr/bin/env python

##
#   Project: gespeaker - A GTK frontend for espeak  
#    Author: Fabio Castelli (Muflone) <muflone@vbsimple.net>
# Copyright: 2009-2015 Fabio Castelli
#   License: GPL-2+
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation; either version 2 of the License, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
##

from distutils.core import setup
from distutils.command.install_data import install_data
from distutils.dep_util import newer
from distutils.log import info
from glob import glob
import os
import sys

class InstallData(install_data):
  def run (self):
    self.install_translations()
    install_data.run(self)

  def install_translations(self):
    info('Installing translations...')
    for po in glob(os.path.join('po', '*.po')):
      lang = os.path.basename(po[:-3])
      mo = os.path.join('build', 'mo', lang, 'gespeaker.mo')

      directory = os.path.dirname(mo)
      if not os.path.exists(directory):
        info('creating %s' % directory)
        os.makedirs(directory)

      cmd = 'msgfmt -o %s %s' % (mo, po)
      info('compiling %s -> %s' % (po, mo))
      if os.system(cmd) != 0:
        raise SystemExit('Error while running msgfmt')

      dest = os.path.join('share', 'locale', lang, 'LC_MESSAGES')
      self.data_files.append((dest, [mo]))


setup(
  name='Gespeaker',
  version='0.8.6',
  description='A GTK+ frontend for eSpeak and mbrola',
  author='Fabio Castelli',
  author_email='muflone@vbsimple.net',
  url='http://www.muflone.com/gespeaker/',
  license='GPL v2',
  scripts=['gespeaker'],
  data_files=[
    ('share/applications', ['data/gespeaker.desktop']),
    ('share/gespeaker/data', ['data/testing.wav']),
    ('share/gespeaker/data/icons', glob('data/icons/*')),
    ('share/gespeaker/data/ui', glob('data/ui/*.glade')),
    ('share/doc/gespeaker', ['doc/README', 'doc/changelog', 'doc/translators']),
    ('share/doc/gespeaker/dbus', glob('doc/dbus/*')),
    ('share/man/man1', ['man/gespeaker.1']),
    ('share/gespeaker/plugins/plugin_dbus', glob('plugins/plugin_dbus/*')),
    ('share/gespeaker/plugins/plugin_debug', glob('plugins/plugin_debug/*')),
    ('share/gespeaker/plugins/plugin_stoponquit', glob('plugins/plugin_stoponquit/*')),
    ('share/gespeaker/plugins/plugin_voicesettings', glob('plugins/plugin_voicesettings/*')),
    ('share/gespeaker/plugins/plugin_welcome', glob('plugins/plugin_welcome/*')),
    ('share/gespeaker/plugins/plugin_windowposition', glob('plugins/plugin_windowposition/*')),
    ('share/gespeaker/plugins/plugin_windowsize', glob('plugins/plugin_windowsize/*')),
    ('share/gespeaker/src', glob('src/*.py'))
  ],
  cmdclass={'install_data': InstallData}
)
